package model;



public class DataSeatPrice implements DataSeatPriceManager{
	public final static int[][] ticketPrices = {
	
//	  			0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19 
	/*A=0**/ {0,-1, -1, 10, 10, 10, 10, 10, 10, -1, -1, 10, 10, 10, 10, 10, 10, 10, 10, -1, -1},
	/*B=1**/ {0,-1, 10, 10, 10, 10, 10, 10, 10, -1, -1, 10, 10, 10, 10, 10, 10, 10, 10, 10, -1},
	/*C=2**/ {0,10, 10, 10, 10, 10, 10, 10, 10, -1, -1, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10},
	/*D=3**/ {0,10, 10, 20, 20, 20, 20, 20, 20, -1, -1, 20, 20, 20, 20, 20, 20, 20, 20, 10, 10},
	/*E=4**/ {0,10, 10, 20, 20, 20, 20, 20, 20, -1, -1, 20, 20, 20, 20, 20, 20, 20, 20, 10, 10},
	/*F=5**/ {0,10, 10, 20, 20, 20, 20, 20, 20, -1, -1, 20, 20, 20, 20, 20, 20, 20, 20, 10, 10},
	/*G=6**/ {0,20, 20, 20, 30, 30, 30, 30, 30, -1, -1, 30, 30, 30, 30, 30, 30, 30, 20, 20, 20},
	/*H=7**/ {0,30, 30, 30, 40, 40, 40, 40, 40, -1, -1, 40, 40, 40, 40, 40, 40, 40, 30, 30, 30},
	/*I=8**/ {0,30, 30, 30, 40, 40, 40, 40, 40, -1, -1, 40, 40, 40, 40, 40, 40, 40, 30, 30, 30},
	/*L=9**/ {0,30, 30, 30, 40, 40, 40, 40, 40, -1, -1, 40, 40, 40, 40, 40, 40, 40, 30, 30, 30},
	/*M=10**/ {0,30, 30, 30, 40, 40, 40, 40, 40, -1, -1, 40, 40, 40, 40, 40, 40, 40, 30, 30, 30},
	/*N=11**/ {0,30, 30, 30, 40, 40, 40, 40, 40, -1, -1, 40, 40, 40, 40, 40, 40, 40, 30, 30, 30},
	/*O=12**/ {0,40, 40, 50, 50, 50, 50, 50, 50, -1, -1, 50, 50, 50, 50, 50, 50, 50, 40, 40, 40},
	/*P=13**/ {0,40, 40, 50, 50, 50, 50, 50, 50, -1, -1, 50, 50, 50, 50, 50, 50, 50, 40, 40, 40},
	/*Q=14**/ {0,40, 40, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 40, 40, 40},
	};
	
//	//	  1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20	
//	{0,1, 1, 10, 10, 10, 10, 10, 10, 1, 1, 10, 10, 10, 10, 10, 10, 10, 10, 1, 1},	//a
//	{0,1, 10, 10, 10, 10, 10, 10, 10, 1, 1, 10, 10, 10, 10, 10, 10, 10, 10, 10, 1},	//b
//	{0,10, 10, 10, 10, 10, 10, 10, 10, 1, 1, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10},	//c
//	{0,10, 10, 20, 20, 20, 20, 20, 20, 1, 1, 20, 20, 20, 20, 20, 20, 20, 20, 10, 10},	//d
//	{0,10, 10, 20, 20, 20, 20, 20, 20, 1, 1, 20, 20, 20, 20, 20, 20, 20, 20, 10, 10},	//e
//	{0,10, 10, 20, 20, 20, 20, 20, 20, 1, 1, 20, 20, 20, 20, 20, 20, 20, 20, 10, 10},	//f
//	{0,20, 20, 20, 30, 30, 30, 30, 30, 1, 1, 30, 30, 30, 30, 30, 30, 30, 20, 20, 20},	//g
//	{0,30, 30, 30, 40, 40, 40, 40, 40, 1, 1, 40, 40, 40, 40, 40, 40, 40, 30, 30, 30},	//h
//	{0,30, 30, 30, 40, 40, 40, 40, 40, 1, 1, 40, 40, 40, 40, 40, 40, 40, 30, 30, 30},	//i
//	{0,30, 30, 30, 40, 40, 40, 40, 40, 1, 1, 40, 40, 40, 40, 40, 40, 40, 30, 30, 30},	//l
//	{0,30, 30, 30, 40, 40, 40, 40, 40, 1, 1, 40, 40, 40, 40, 40, 40, 40, 30, 30, 30},	//m
//	{0,30, 30, 30, 40, 40, 40, 40, 40, 1, 1, 40, 40, 40, 40, 40, 40, 40, 30, 30, 30},	//n
//	{0,40, 40, 40, 50, 50, 50, 50, 50, 1, 1, 50, 50, 50, 50, 50, 50, 50, 40, 40, 40},	//o
//	{0,40, 40, 40, 50, 50, 50, 50, 50, 1, 1, 50, 50, 50, 50, 50, 50, 50, 40, 40, 40},	//p
//	{0,40, 40, 40, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 40, 40, 40}	//q
//};


	@Override
	public int getSeatPrice(int row, int colum) {
		return ticketPrices[row][colum];
	}
}
package view;
import javax.swing.JFrame;

import model.DataSeatPrice;
import model.Seat;
import model.TheaterManagement;
import controller.TheaterSystemGui;


public class Main {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DataSeatPrice dataSeat = new DataSeatPrice();
		Seat seat = new Seat(dataSeat);
		TheaterManagement manage = new TheaterManagement(seat.getSeatPrice());
		TheaterSystemGui gui = new TheaterSystemGui(manage);
		
		gui.setSize(940, 600);
		gui.setLocation(210, 80);
		gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		gui.setVisible(true);
	}
}
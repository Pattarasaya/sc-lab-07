package controller;


import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



import javax.swing.table.*;

import model.TheaterManagement;


public class TheaterSystemGui extends JFrame {
	JPanel panel = new JPanel();
	DefaultTableModel dataModel = new DefaultTableModel();
	JTable table = new JTable(dataModel);
	TheaterManagement theater;

	public TheaterSystemGui (TheaterManagement t) {
		this.theater = t;
		String[] columnlist = {"","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"};
		Object[][] data = {{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}};
		final String[] rowlist = {"A","B","C","D","E","F","G","H","I","L","M","N","O","P","Q"};
		for (int col = 0; col < columnlist.length; col++){
			dataModel.addColumn(columnlist[col]);
		}

		for (int row = 0; row < 15; row++) {
			dataModel.addRow(data[row]);
		}
		for(int i = 0; i < 15; i++){
			for(int j = 1; j < 21; j++){
				dataModel.setValueAt(theater.getTicketPrices()[i][j], i, j);
				dataModel.setValueAt(rowlist[i],i,0);
			}
		}
//		table.setOpaque(true);
//		table.setFillsViewportHeight(true);
//		table.setBackground(Color.white);
		table.getColumnModel().getColumn(0).setCellRenderer(new RowHeaderRenderer());
		//Create the scroll pane and add the table to it.
		final JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 60, 900, 263);

		//Total Price
		final JLabel total = new JLabel("Total Price :  " + theater.getTotal());
		total.setBounds(200, 500, 160, 20);
		total.setFont(new Font("Serif",Font.BOLD,20));

		// Row
		JLabel rowLabel = new JLabel("Row :");
		rowLabel.setBounds(120, 370, 100, 20);
		rowLabel.setFont(new Font("Serif",Font.BOLD,20));
		final JTextField rowfield = new JTextField();
		rowfield.setBounds(180, 366, 100, 30);
		rowfield.setFont(new Font("Serif",Font.BOLD,20));

		// Column
		JLabel columnLabel = new JLabel("Column :");
		columnLabel.setBounds(370, 370, 100, 20);
		columnLabel.setFont(new Font("Serif",Font.BOLD,20));
		final JTextField columnField = new JTextField();
		columnField.setBounds(450, 366, 100, 30);
		columnField.setFont(new Font("Serif",Font.BOLD,20));

		//Search
		JLabel searchprice = new JLabel("Search By Price :");
		searchprice.setBounds(120, 430, 160, 20);
		searchprice.setFont(new Font("Serif",Font.BOLD,20));
		final JTextField field3 = new JTextField("0");
		field3.setBounds(300, 426, 100, 30);
		field3.setFont(new Font("Serif",Font.BOLD,20));
		JLabel label = new JLabel("**��� 0 = ���Ҥҷ����� **");
		label.setBounds(200, 460, 300, 20);
		label.setFont(new Font("Serif",Font.BOLD,10));
		
		//Buy Button
		JButton buy = new JButton("Buy");
		buy.setBounds(620, 365, 200, 100);
		buy.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg1) {
				// TODO Auto-generated method stub
				theater.getSeatPrice(Integer.valueOf(rowfield.getText()), Integer.valueOf(columnField.getText()));
				theater.setSelectPrice(Integer.valueOf(field3.getText()));
				total.setText("Total Price :  " + theater.getTotal());
				for(int i = 0; i < 15; i++){
					for(int j = 1; j < 21; j++){
						dataModel.setValueAt(theater.getTicketPrices()[i][j], i, j);
					}
				}
			}
		});

		JButton button2 = new JButton("Search");
		button2.setBounds(450, 426, 100, 30);
		button2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {
				// TODO Auto-generated method stub
				theater.setSelectPrice(Integer.valueOf(field3.getText()));
				for(int i = 0; i < 15; i++){
					for(int j = 1; j < 21; j++){
						dataModel.setValueAt(theater.getTicketPrices()[i][j], i, j);
					}
				}
			}
		});

		//Add to panel.
		panel.add(scrollPane);
		panel.add(rowLabel);
		panel.add(rowfield);
		panel.add(columnLabel);
		panel.add(columnField);
		panel.add(searchprice);
		panel.add(field3);
		panel.add(total);
		panel.add(label);
		panel.add(buy);
		panel.add(button2);
		panel.setLayout(null);
		getContentPane().add(panel);
	}


	static class RowHeaderRenderer extends DefaultTableCellRenderer {
		public RowHeaderRenderer() {
			setHorizontalAlignment(JLabel.CENTER);
		}

		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			if (table != null) {
				JTableHeader header = table.getTableHeader();

				if (header != null) {
					setForeground(header.getForeground());
					setBackground(header.getBackground());
					setFont(header.getFont());
				}
			}

			if (isSelected) {
				setFont(getFont().deriveFont(Font.BOLD));
			}

			setValue(value);
			return this;
		}
	}
} 
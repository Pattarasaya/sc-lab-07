package controller;

import model.TheaterManagement;


public class Controller {
	TheaterManagement theater;
	public Controller(TheaterManagement manage){
		this.theater = manage;
	}
	
	public void setSelectPrice(int price){
		theater.setSelectPrice(price);
	}
	
	public int[][] getTicketPrices(){
		return theater.getTicketPrices();
	}
	public int getSeatPrice(int row, int colum){
		return theater.getSeatPrice(row, colum);
	}
}
